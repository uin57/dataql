---
id: v4.1.4
title: v4.1.4 (2020-04-30)
---

# v4.1.4 (2020-04-30)

## 新增
- 接口可以跨域访问。
- Dataway 增加 `CompilerSpiListener` 扩展点，可以自定义 DataQL 编译过程。
- Dataway 增加 `PreExecuteChainSpi` 扩展点，可以在 DataQL 执行之前进行干预。配合 `ResultProcessChainSpi` 可以实现缓存和权限。
- Dataway 增加 `ResultProcessChainSpi` 扩展点，可以对DataQL执行的结果进行二次处理。
- Dataway 新增 `DatawayService` 界面配置的接口可以在本地应用上用代码发起调用了。
- [Issue 13](https://github.com/zycgit/hasor/issues/13) Dataway 支持配置多个数据源。但一个 DataQL 查询中目前依然只能使用一种数据源。
- [Issue I1F0ZB](https://gitee.com/zycgit/hasor/issues/I1F0ZB) Dataway 新增 Oracle 的支持。
- 新增 `FRAGMENT_SQL_COLUMN_CASE` 选项，可以决定 SQL 执行器的返回结果 key 策略，是全部大写还是全部小写或者满足驼峰。
- 新增 `mapKeyToLowerCase`、`mapKeyToUpperCase`、`mapKeyToHumpCase` 三个函数，对 Map 的 Key 做转换。

## 优化
- [Issue I1EUAL](https://gitee.com/zycgit/hasor/issues/I1EUAL) 改进 Dataway 在处理 GET 请求时，多个同名参数获取的问题。
- 之前只能拿到数组形态，在于 POST 模式进行对比的时容易产生奇异造成认为是 Bug 的假象。
- [Issue I1DK6R](https://gitee.com/zycgit/hasor/issues/I1DK6R) hasor-dataql-fx 项目中 ognl 内嵌到 jar包中，减少两个外部依赖 jar。
- hasor-web 支持路径中出现多个连续 `/` ，例如： `http://127.0.0.1:8080/app/////interface-ui/#/new` 。连续的 / 会被折叠成一个。
- Dataway UI 界面中模式切换会因为 `//` 但行注释问题产生一些不友好的用户体验。现改成 /**/ 多行注释方式。

## 修复
- [Issue I1EM2V](https://gitee.com/zycgit/hasor/issues/I1EM2V) Dateway 4.1.3 版本资源文件缺失问题。
- [Issue I1FD95](https://gitee.com/zycgit/hasor/issues/I1FD95) Dataway 修复 spring boot context_path 不支持的问题。
- Dataway 当关闭 UI 功能之后接口调用报 NPE 问题。Bug 原因是 Dataway 内置 DataQL 的环境是一个隔离环境，隔离环境的初始化是在 UI 之后。
- 修复 `SqlFragment` 单行注释判断不识别的问题。