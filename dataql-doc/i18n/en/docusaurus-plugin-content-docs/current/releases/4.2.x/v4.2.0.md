---
id: v4.2.0
title: v4.2.0 (2020-09-22)
---

# v4.2.0 (2020-09-22)

## 新增
- 新增 `FRAGMENT_SQL_MULTIPLE_QUERIES` hint，这个 hint 可以处理多条 SQL 同时执行时结果的保留策略。
- 新增 `LookupConnectionListener` SPI 功效和 `LookupDataSourceListener` 类似只不过是进一步到 Connection。
- 新增 `FxSqlCheckChainSpi` 扩展点可以用于执行 `@@sql` 时对 SQL 进行检查和改写。
- Dataway 沉淀 dataAccessLayer 元信息访问层
- 增加 `HASOR_DATAQL_DATAWAY_DAL_TYPE` 环境变量用于选择 dataway 元信息存储器类型。
- `dataql-fx-hconfig.xml` 中 Fragment 提升到配置文件中配置。
- `dataway-hconfig.xml` 中 `dataAccessLayer` 可以配置文件定义不同的 provider 然后利用 `HASOR_DATAQL_DATAWAY_DAL_TYPE` 来选择。
- 新增 nacos 的支持，利用 nacos 来充当接口配置信息的存储器。

## 优化
- [Issue I1M93Z](https://gitee.com/zycgit/hasor/issues/I1M93Z) 支持多条 SQL 语句同时执行。
- [Issue I1RJQK](https://gitee.com/zycgit/hasor/issues/I1RJQK) 在使用 springboot 集成dataway 启动时报 unknown DataBaseType -> 使用版本是 4.1.13
- [Issue I1PY5O](https://gitee.com/zycgit/hasor/issues/I1PY5O) 能保存接口，但是发布接口报超长
- [Issue I1M9SU](https://gitee.com/zycgit/hasor/issues/I1M9SU) 如果数据库不支持主键自增，则会导致数据库api混乱，从而导致生成的swagger有误
- [Issue I1HN6U](https://gitee.com/zycgit/hasor/issues/I1HN6U) oracle 11g版本创表语句无法执行。
- SqlFragment 迎来重大升级。弱化了 SqlMode 的作用，允许执行其它类型的 SQL。 因此例如：存储过程的在 4.1.14 版本中得到了支持。
- Dataway - release 表、info 表 结构调整，升级脚本见本文末尾
- 所有字段内容格式均为 JsonSchema 格式。
- headerData 改名为 requestHeader，内容格式从 JSON 对象改为 JSON 字符串。
- 新增 responseBody、responseHeader 两个字段，内容也是 JSON 字符串。
- 接口发布历史中，增加状态显示。
- 删除 `HASOR_DATAQL_DATAWAY_FORCE_DBTYPE` 环境变量及其功能。
- 删除 DataQL 中默认全局变量 dbMapping。
- 删除 DataBaseMapping 枚举。

## 修复
- 修复新增接口保存成功之后，无法点击冒烟和发布的问题。受影响所有 Editor 状态的接口都会无法冒烟和发布。
- 解决 `DatawayFinder` 可能导致的循环依赖问题。

## Dataway Dataway 4.1.5~4.1.13 数据库升级脚本

- 建议升级之前进行数据备份（Mysql 升级脚本）

```sql
/* 默认数据订正 */
update interface_info set api_comment = '' where api_comment is null;
update interface_info set api_schema = '{}' where api_schema is null;
update interface_info set api_sample = '{}' where api_sample is null;
update interface_info set api_option = '{}' where api_option is null;
update interface_info set api_create_time = now() where api_create_time is null;
update interface_info set api_gmt_time = now() where api_gmt_time is null;

update interface_release set pub_script = '' where pub_script is null;
update interface_release set pub_script_ori = '' where pub_script_ori is null;
update interface_release set pub_schema = '{}' where pub_schema is null;
update interface_release set pub_sample = '{}' where pub_sample is null;
update interface_release set pub_option = '{}' where pub_option is null;
update interface_release set pub_release_time = now() where pub_release_time is null;

/* 表结构变更 */
alter table interface_info
    modify api_id varchar(64) not null comment 'ID';
alter table interface_info
    modify api_status varchar(4) not null comment '状态：0，1，2，3';
alter table interface_info
	modify api_comment varchar(255) not null comment '注释';
alter table interface_info
	modify api_schema mediumtext not null comment '接口的请求/响应数据结构';
alter table interface_info
	modify api_sample mediumtext not null comment '请求/响应/请求头样本数据';
alter table interface_info
	modify api_option mediumtext not null comment '扩展配置信息';
alter table interface_info
	modify api_create_time varchar(32) not null comment '创建时间';
alter table interface_info
	modify api_gmt_time varchar(32) not null comment '修改时间';

alter table interface_release
    modify pub_id varchar(64) not null comment 'Publish ID';
alter table interface_release
    modify pub_api_id varchar(64) not null comment '所属API ID';
alter table interface_release
    modify pub_status varchar(4) not null comment '状态：0有效，1无效（可能被下线）';
alter table interface_release
    modify pub_script mediumtext not null comment '查询脚本：xxxxxxx';
alter table interface_release
    modify pub_script_ori mediumtext not null comment '原始查询脚本，仅当类型为SQL时不同';
alter table interface_release
    modify pub_schema mediumtext not null comment '接口的请求/响应数据结构';
alter table interface_release
    modify pub_sample mediumtext not null comment '请求/响应/请求头样本数据';
alter table interface_release
    modify pub_option mediumtext not null comment '扩展配置信息';
alter table interface_release
    modify pub_release_time varchar(32) not null comment '发布时间（下线不更新）';
alter table interface_release
	add pub_comment varchar(255) default '' not null comment '备注';
alter table interface_release
    modify pub_comment varchar(255) not null comment '备注';

/* 重建索引 */
alter table interface_info drop key idx_interface_info;
alter table interface_info
	add constraint idx_interface_info
		unique (api_path);
create index idx_interface_release_path
	on interface_release (pub_path);

/* 时间日期数据订正 */
update interface_info set
    api_create_time = UNIX_TIMESTAMP(STR_TO_DATE(api_create_time, '%Y-%m-%d %H:%i:%s')) * 1000,
    api_gmt_time = UNIX_TIMESTAMP(STR_TO_DATE(api_gmt_time, '%Y-%m-%d %H:%i:%s')) * 1000;
update interface_release set
    pub_release_time = UNIX_TIMESTAMP(STR_TO_DATE(pub_release_time, '%Y-%m-%d %H:%i:%s')) * 1000;
```