---
id: v4.2.3
title: v4.2.3 (2021-02-26)
---

# v4.2.3 (2021-02-26)

## 新增
- 增加 `assert` 命令，可以用 DataQL 来充当测试场景下断言功能。
- [Issue I3558A](https://gitee.com/zycgit/hasor/issues/I3558A) 支持 swagger2.json 手动设置验证

## 优化
- dataql-fx 中的访谈注册器能力沉淀到 `SqlDialectRegister` 上。
- 删除 dataway 新版本检测能力，保留 git 挂件展示。
- [Issue I37MMP](https://gitee.com/zycgit/hasor/issues/I37MMP) 修复自定义 `LoginTokenChainSpi` 和 `LoginPerformChainSpi` 不生效的问题。
- [Issue I2W8Z5](https://gitee.com/zycgit/hasor/issues/I2W8Z5) 修复对 oracle 类型兼容问题。该问题是由于 4.2.2 中 hasor-db 加入 typeHandler 时引入的。
- `AuthorizationType` 拆分为 `PermissionGroup`、`PermissionType`
- `AuthorizationChainSpi` 的参数优化一下，之前的参数使用起来不太方便。
- `PerformController` 的权限单独拆出来。
- 重构异常体系，所有 DataQL 异常都派生自 `DataQueryException`。
- 拆分 compiler 包，将 ast 和 passer 部分独立出来。形成 passer、compiler、runtime 三大组件包。
- responseFormat 部分在异常的时候 message 中代码所处行号等信息，单独拆分到 location 中。
- HttpParameters 增加 invokerLocal，同时 FxWebInterceptor 做简化处理。
