---
id: v4.1.1
title: v4.1.1 (2020-02-22)
---

# v4.1.1 (2020-02-22)

## 新增
- 新增 DataQL Maven 插件，会根据 `*.ql` 文件生成对应的 Java 调用代码。
- DataQL：`Finder` 接口取消 `Object findBean(String beanName)`  方法

## 优化
- `UdfSourceAssembly` 接口优化实现，getSupplier 改为返回自己。
- `UdfSourceAssembly` 接口中：`Object`、`UdfSource`、`UdfSourceAssembly` 三个类型的方法不被默认列入。
- `SqlQueryFragment` 当遇到返回数据仅一行时，将不在包裹 List 。
- `CollectionUdfSource evalJoinKey` 方法兼容 NULL 值。
- `NumberDOP` 在做二元计算时，兼顾了 `POSITIVE_INFINITY`、`NaN`、`NEGATIVE_INFINITY` 三种情况。
- DO 指令增加了 除法修正 的前置处理
